#!/usr/bin/env groovy
@Library('build-tools')

def dummy

node {
    def clusterNameToDeploy = ""
    try {
        clusterNameToDeploy = CLUSTER_NAME
    } catch (Throwable e) {
        error("Build failed because no cluster name specified")
        return
    }

    def clusterServiceToDeploy = ""
    try {
        clusterServiceToDeploy = CLUSTER_SERVICE
    } catch (Throwable e) {
        error("Build failed because no cluster service specified")
        return
    }

    def dockerImageURL = ""
    try {
        dockerImageURL = DOCKER_IMAGE_URL
    } catch (Throwable e) {
        error("Build failed because no docker image url specified")
        return
    }

    echo "Building { dockerImageURL: ${dockerImageURL}, clusterNameToDeploy: ${clusterNameToDeploy}, " +
            "clusterServiceToDeploy: ${clusterServiceToDeploy} }"

    stage("Deploy new image to ${clusterServiceToDeploy}") {
        ecsDeploy {
            clusterName = "${clusterNameToDeploy}"
            clusterService = "${clusterServiceToDeploy}"
            image = "${dockerImageURL}"
        }
    }
}