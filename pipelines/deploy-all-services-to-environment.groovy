#!/usr/bin/env groovy
@Library('build-tools')
import com.coral.epos2.Utils

node {
    def environmentName = ""
    try {
        environmentName = ENVIRONMENT_NAME
    } catch (Throwable e) {
        error("Build failed because no environment specified")
        return
    }

    def dockerVersion = ""
    try {
        dockerVersion = DOCKER_VERSION
    } catch (Throwable e) {
        dockerVersion = "develop"
    }

    def repoBase = '550357753517.dkr.ecr.eu-west-1.amazonaws.com'
    def clusterName = "coral-epos2-${environmentName}-cluster"

    echo "Build ${Utils.serviceConfigs.size()} service(s) to env: ${environmentName}"

    for (serviceConfig in Utils.serviceConfigs) {
        stage('Deploy ' + serviceConfig.name) {
            ecsDeploy {
                clusterName = "${clusterName}"
                clusterService = "coral-epos2-${environmentName}-${serviceConfig.name}"
                image = "${repoBase}/${serviceConfig.repo}:${dockerVersion}"
            }
        }
    }
}