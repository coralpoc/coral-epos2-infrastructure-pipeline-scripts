#!/usr/bin/groovy
package com.coral.epos2

import com.cloudbees.groovy.cps.NonCPS

def serviceConfigs = [
        [name: 'customers-service', dockerRepo: 'coral-epos2-customers', gitRepo: 'coral-epos2-customers'],
        [name: 'betting-service', dockerRepo: 'coral-epos2-betting-service', gitRepo: 'coral-epos2-betting'],
        [name: 'cashflow-service', dockerRepo: 'coral-epos2-cashflow', gitRepo: 'coral-epos2-cashflow'],
        [name: 'users-service', dockerRepo: 'coral-epos2-users', gitRepo: 'coral-epos2-users'],
        [name: 'systems-service', dockerRepo: 'coral-epos2-systems-service', gitRepo: 'coral-epos2-system-service'],
        [name: 'audit-service', dockerRepo: 'coral-epos2-audit-service', gitRepo: 'coral-epos2-audit'],
        [name: 'events-service', dockerRepo: 'coral-epos2-events', gitRepo: 'coral-epos2-events'],
        [name: 'notifications-service', dockerRepo: 'coral-epos2-notifications-service', gitRepo: 'coral-epos2-notifications'],
        [name: 'queue-log', dockerRepo: 'coral-epos2-queue-log', gitRepo: 'coral-epos2-queue-log'],
        [name: 'test-ctrl', dockerRepo: 'coral-epos2-test-screen', gitRepo: 'coral-epos2-test-screen']
]

@NonCPS
def getServiceConfig(name) {
    for (serviceConfig in serviceConfigs) {
        if (serviceConfig.gitRepo == name) {
            return serviceConfig
        }
    }

    return null
}

def getDefaultCIChannel() {
    return "#jenkins-ci"
}


def getSigningChannel() {
    return "#signed-builds"
}

def notifySigningRequired(String channelName, String jobName, String buildNumber, String buildUrl) {
    notifySlack("${channelName}", "#FF0000", "SIGNING REQUIRED: Job '${jobName} [${buildNumber}]' (${buildUrl})")
}

def notifySlackBuildFailed(String channelName, String jobName, String buildNumber, String buildUrl) {
    notifySlack("${channelName}", "#FF0000", "FAILED: Job '${jobName} [${buildNumber}]' (${buildUrl})")
}

def notifySlack(String channelName, String color, String message) {
    slackSend(channel: "${channelName}", color: "${color}", message: "${message}")
}

return this;