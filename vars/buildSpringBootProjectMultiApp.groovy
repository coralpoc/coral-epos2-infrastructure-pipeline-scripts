#!/usr/bin/groovy

import com.coral.epos2.Utils

/**
 * Groovy script to build multiple spring boot projects and deploy the services if necessary
 *
 * Requires a service deployment config in the format:
 *     serviceConfigs = [
 *        'epos2-events-api-app': [
 *           dockerRepo  : 'coral-epos2-events-api',
 *            environments: [
 *                [
 *                    name             : 'dev1',
 *                    branch           : 'develop',
 *                    autoDeploy       : true,
 *                    clusterName      : 'coral-epos2-dev1-cluster',
 *                    serviceName      : 'dev1-events-service-api',
 *                    version          : 'develop',
 *                    awsProfile       : 'epos2dev'
 *                ]
 *            ]
 *       ],
 *        'epos2-events-api-app': [
 *           dockerRepo  : 'coral-epos2-events-api',
 *            environments: [
 *                [
 *                    name             : 'dev1',
 *                    branch           : 'develop',
 *                    autoDeploy       : true,
 *                    clusterName      : 'coral-epos2-dev1-cluster',
 *                    serviceName      : 'dev1-events-service-api',
 *                    version          : 'develop',
 *                    awsProfile       : 'epos2dev'
 *                ]
 *            ]
 *        ]
 *     ]
 *
 */
def call(body) {
    // evaluate the body block, and collect configuration into the object

    def config = [:]
    def utils = new Utils()
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    def jobName = null
    def jobNames = env.JOB_NAME.split('/')
    for (name in jobNames) {
        if (name.startsWith('epos2-')) {
            jobName = name
            break
        }
    }

    def serviceConfig = config.serviceConfigs[jobName]

    echo "buildSpringBootProjectMultiApp (${jobName}): ${serviceConfig.toMapString()}"

    node('Epos2') {
        try {
            defaultBuildProperties {}

            def MVN_HOME = tool name: 'Maven-3.3.9', type: 'hudson.tasks.Maven$MavenInstallation'

            stage('Checkout') {
                checkout scm
            }

            stage('Build') {
                sh "${MVN_HOME}/bin/mvn clean install -Dgit.branch.name=${env.BRANCH_NAME}"
            }

            if (env.BRANCH_NAME == 'develop') {
                stage('SonarQube analysis') {
                    withSonarQubeEnv('Sonarqube') {
                        // requires SonarQube Scanner for Maven 3.2+
                        sh "${MVN_HOME}/bin/mvn clean org.jacoco:jacoco-maven-plugin:prepare-agent install -Dgit.branch.name=${env.BRANCH_NAME}"
                        sh "${MVN_HOME}/bin/mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.2:sonar"
                    }
                }
            }

            deployToEnvironment {
                projectType = 'java'
                appDir = jobName
                dockerRepo = serviceConfig.dockerRepo
                environments = serviceConfig.environments
            }

            echo "Cleaning up workspace"
            deleteDir()
        } catch (error) {
            // send default error to main ci channel
            utils.notifySlackBuildFailed(utils.getDefaultCIChannel(), "${env.JOB_NAME}", "${env.BUILD_NUMBER}", "${env.BUILD_URL}")

            // try and send error message to a channel matching the branch name
            utils.notifySlackBuildFailed("${env.BRANCH_NAME}".toLowerCase(), "${env.JOB_NAME}", "${env.BUILD_NUMBER}", "${env.BUILD_URL}")

            throw error
        }
    }
}
