#!/usr/bin/groovy

def call(body) {
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "notifyFailed: ${config.toString()}"

    def channelName = config.channelName
    def jobName = config.jobName
    def buildNumber = config.buildNumber
    def buildUrl = config.buildUrl

    slackSend(channel: "${channelName}", color: '#FF0000', message: "FAILED: Job '${jobName} [${buildNumber}]' (${buildUrl})")
}