#!/usr/bin/groovy

def call(body) {
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "buildDockerImage: ${config.toString()}"

    def dockerImage
    def dockerVersion = config.dockerVersionName // Which version of the docker image needs to be deployed
    def repoBase = config.repoBase // base name for the coral-epos2 repo account
    def repoName = config.repoName // Name of the service repo to push new docker image to
    def dockerCredentialsId = config.dockerCredentialsId // aws ecs login - see jenkins global credentials

    echo 'Baking docker image'
    dockerImage = docker.build("${repoName}:${dockerVersion}")

    echo 'Deploying to docker repo'
    docker.withRegistry("https://${repoBase}", "${dockerCredentialsId}") {
        dockerImage.push("${dockerVersion}")
    }
}