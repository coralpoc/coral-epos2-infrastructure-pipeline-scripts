def call(body) {
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "deployDockerProject: ${config.serviceConfig.toMapString()}"

    def environmentsToBuild = []

    // go through environment configs and check what needs to be deployed
    for (environment in config.serviceConfig.environments) {
        echo "Found environment config: ${environment.toString()}"

        if (environment.branch == env.BRANCH_NAME) {
            echo "Environment config set to build from branch: ${environment.toString()}"
            environmentsToBuild.push(environment)
        }
    }

    // if we have environments to build to then create image
    if (!environmentsToBuild.empty) {
        def repoBaseToDeploy = "550357753517.dkr.ecr.eu-west-1.amazonaws.com"
        def repoNameToDeploy = config.serviceConfig.dockerRepo
        def versionNameToDeploy = "${env.BRANCH_NAME}"
        versionNameToDeploy = versionNameToDeploy.replaceAll('/','-')

        stage ('Building docker image') {
            buildDockerImage {
                repoBase = "${repoBaseToDeploy}"
                repoName = "${repoNameToDeploy}"
                dockerCredentialsId = "ecr:eu-west-1:aws-ops-credentials"
                dockerVersionName = "${versionNameToDeploy}"
            }
        }

        for (environment in environmentsToBuild) {
            // if this branch is configured to deploy to the environment then deploy
            if (environment.autoDeploy) {
                echo "Environment config set to deploy from branch..."

                def awsProfileToUse = environment.awsProfile
                def serviceNameToDeploy = environment.serviceName
                def clusterNameToDeploy = environment.clusterName
                def customdeploy = environment.customdeploy

                stage('Deploy ' + environment.name) {
                    // if there is a custom build script then just call it, this is to handle old legacy ec2 builds
                    if (customdeploy) {
                        sh "${customdeploy}"
                    } else {
                        deployService {
                            awsProfile = awsProfileToUse
                            serviceName = serviceNameToDeploy
                            clusterName = clusterNameToDeploy
                            imageName = "${repoBaseToDeploy}/${repoNameToDeploy}:${versionNameToDeploy}"
                        }
                    }
                }
            } else {
                echo "Environment config not set to deploy from branch, skipping"
            }
        }
    }
}
