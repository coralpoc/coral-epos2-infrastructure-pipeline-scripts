#!/usr/bin/groovy

def call(body) {
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "buildElectronArtifactAndDeployToS3: ${config.toString()}"

    def environment = "${config.environment}"
    def hostname = "${config.hostname}"
    def socketsProtocol = "${config.socketsProtocol}"
    def apiProtocol = "${config.apiProtocol}"
    def releaseFolder = "${config.releaseFolder}"
    def bucketName = "coral-epos2-electron-releases"
    def branchName = env.BRANCH_NAME.replaceAll('/','-')

    echo "branchName: ${branchName}"

    def buildVersion = "${branchName}-build-${env.BUILD_NUMBER}-${config.environment}" // e.g. EPOS-12-build-20-staging
    echo "buildVersion: ${buildVersion}"

    def configFileName = "app/config/env-${environment}.js"
    echo "building config file ${configFileName}"

    writeFile file: "${configFileName}", text: """
        import deepAssign from \'deep-assign\';

        import baseConfig from \'./base\';

        const config = {
          websockets: {
            protocol: \'${socketsProtocol}\',
            hostname: \'${hostname}\',
          },
          api: {
            protocol: \'${apiProtocol}\',
            hostname: \'${hostname}/api\',
          },
        };

        export default Object.freeze(deepAssign({}, baseConfig, config));
    """

    stage("Build electron artifacts configured for ${environment} environment") {
        wrap([$class: 'Xvfb', autoDisplayName: true, debug: true, displayNameOffset: 100, installationName: 'Default Xvfb', parallelBuild: true]) {
            sh "BUILD_VERSION=${buildVersion} CONFIG_ENV=env-${environment} npm run release:all"
            sh "zip -r release/env-${environment}/${buildVersion}-win32-x64.zip release/env-${environment}/win32-x64/Coral\\ EPOS\\ 2-win32-x64"
            sh "zip -r release/env-${environment}/${buildVersion}-darwin-x64.zip release/env-${environment}/darwin-x64/Coral\\ EPOS\\ 2-darwin-x64"
        }
    }

    stage("Deploy ${environment} artifacts to S3 bucket ${bucketName}") {
        step([
                $class                              : 'S3BucketPublisher',
                dontWaitForConcurrentBuildCompletion: false,
                entries                             : [
                        [
                                bucket                 : "${bucketName}${releaseFolder}/${environment}",
                                excludedFile           : '',
                                flatten                : false,
                                gzipFiles              : false,
                                keepForever            : false,
                                managedArtifacts       : false,
                                noUploadOnFailure      : true,
                                selectedRegion         : 'eu-west-1',
                                showDirectlyInBrowser  : false,
                                sourceFile             : "release/env-${environment}/*.zip",
                                storageClass           : 'STANDARD',
                                uploadFromSlave        : false,
                                useServerSideEncryption: false
                        ]
                ],
                profileName                         : 'Jenkins S3 Profile',
                userMetadata                        : []
        ])
    }
}