#!/usr/bin/groovy

import com.coral.epos2.Utils

/**
 * Groovy script to build a spring boot project and deploy a service if necessary
 *
 * Requires a service deployment config in the format:
 *     serviceConfig = [
 *          name        : 'my-service',
 *          repoName    : 'my-service-repo',
 *          environments: [
 *              [
 *                  environment: 'dev',
 *                  autoDeploy       : true,
 *                  clusterName      : 'my-dev-services',
 *                  serviceName      : 'my-service',
 *                  dockerVersionName: 'develop'
 *              ],
 *              [
 *                  environment      : 'test1',
 *                  autoDeploy       : true,
 *                  clusterName      : 'my-test1-services',
 *                  serviceName      : 'my-service',
 *                  dockerVersionName: 'develop'
 *              ],
 *              [
 *                  environment      : 'staging',
 *                  autoDeploy       : false,
 *                  clusterName      : 'my-staging-services',
 *                  serviceName      : 'my-service',
 *                  dockerVersionName: 'master'
 *              ]
 *          ]
 *      ]
 *
 */
def call(body) {
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    def utils = new Utils()
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    def jobName = null
    def jobNames = env.JOB_NAME.split('/')
    for (name in jobNames) {
        if (name.startsWith('epos2-')) {
            jobName = name
            break
        }
    }

    def serviceConfig = config.serviceConfigs[jobName]

    echo "buildNodeProject (${jobName}): ${serviceConfig.toMapString()}"

    node('Epos2') {
        try {
            defaultBuildProperties {}

            def nodeHome = tool name: 'Node-6.4.0', type: 'jenkins.plugins.nodejs.tools.NodeJSInstallation'

            stage('Checkout') {
                checkout scm
            }

            stage('Build') {
                env.PATH = "${nodeHome}/bin:${env.PATH}"
                sh 'npm install'
            }

            deployToEnvironment {
                appDir = jobName
                dockerRepo = serviceConfig.dockerRepo
                environments = serviceConfig.environments
            }

            echo "Clean up workspace"
            deleteDir()
        } catch (error) {
            // send default error to main ci channel
            utils.notifySlackBuildFailed(utils.getDefaultCIChannel(), "${env.JOB_NAME}", "${env.BUILD_NUMBER}", "${env.BUILD_URL}")

            // try and send error message to a channel matching the branch name
            utils.notifySlackBuildFailed("${env.BRANCH_NAME}".toLowerCase(), "${env.JOB_NAME}", "${env.BUILD_NUMBER}", "${env.BUILD_URL}")

            throw error
        }
    }
}