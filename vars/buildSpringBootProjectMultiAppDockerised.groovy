#!/usr/bin/groovy

/**
 * Groovy script to build multiple spring boot projects and deploy the services if necessary
 *
 * Requires a service deployment config in the format:
 * serviceConfigs = [
 *       'df-streamer-bi': [
 *           node: 'CRM',
 *           dockerRepo  : 'coral-crm-streamer-bi',
 *           deployData  : [
 *               id          : '206873186066.dkr.ecr.eu-west-1.amazonaws.com',
 *               credentials : 'ecr:eu-west-1:ACCOUNT'
 *           ],
 *           environments: [
 *               [
 *                   name             : 'dev1',
 *                   branch           : 'new-build',
 *                   autoDeploy       : true,
 *                   clusterName      : 'coral-crm-dev1-cluster',
 *                   serviceName      : 'dev1-streamer-bi-service',
 *                   version          : 'develop',
 *                   awsCredentials   : 'ACCOUNT'
 *               ]
 *           ]
 *       ]
 *   ]
 *
 */
def call(body) {
    // evaluate the body block, and collect configuration into the object

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    def jobNames = env.JOB_NAME.split('/')
    jobName = jobNames[jobNames.size() - 2]

    def serviceConfig = config.serviceConfigs[jobName]

    echo "buildSpringBootProjectMultiAppDockerised (${jobName}): ${serviceConfig.toMapString()}"

    node(serviceConfig.node) {
        defaultBuildProperties {}

        def MVN_HOME = tool name: 'Maven-3.3.9', type: 'hudson.tasks.Maven$MavenInstallation'

        stage('Checkout') {
            checkout scm
        }

        stage('Build') {
            sh "${MVN_HOME}/bin/mvn clean install -Pdocker"
        }

        if (env.BRANCH_NAME == 'develop') {
            stage('SonarQube analysis') {
                withSonarQubeEnv('Sonarqube') {
                    sh "${MVN_HOME}/bin/mvn clean org.jacoco:jacoco-maven-plugin:prepare-agent install -Dgit.branch.name=${env.BRANCH_NAME}"
                    sh "${MVN_HOME}/bin/mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.2:sonar"
                }
            }
        }

        dir("${jobName}/target/docker") {
            def environmentsToBuild = []

            for (environment in serviceConfig.environments) {
                echo "Found environment config: ${environment.toString()}"

                if (environment.branch == env.BRANCH_NAME) {
                    echo "Environment config set to build from branch: ${environment.toString()}"
                    environmentsToBuild.push(environment)
                }
            }

            if (!environmentsToBuild.empty) {
                def deployData = serviceConfig.deployData
                def repoNameToDeploy = serviceConfig.dockerRepo
                def versionNameToDeploy = "${env.BRANCH_NAME}"

                stage ('Building docker image') {
                    echo 'Deploying to docker repo'
                    def dockerImage = docker.build("${repoNameToDeploy}:${versionNameToDeploy}")
                    docker.withRegistry("https://${deployData.id}/${config.repoName}", "${deployData.credentials}") {
                        dockerImage.push("${versionNameToDeploy}")
                    }
                }

                for (environment in environmentsToBuild) {
                    if (environment.autoDeploy) {
                        echo "Environment config set to deploy from branch..."

                        def awsProfileToUse = environment.awsProfile
                        def jenkinsCredentialsId = environment.awsCredentials
                        def serviceNameToDeploy = environment.serviceName
                        def clusterNameToDeploy = environment.clusterName

                        stage('Deploy ' + environment.name) {
                            deployService {
                                awsProfile = awsProfileToUse
                                jenkinsCredentials = jenkinsCredentialsId
                                serviceName = serviceNameToDeploy
                                clusterName = clusterNameToDeploy
                                imageName = "${repoBaseToDeploy}/${repoNameToDeploy}:${versionNameToDeploy}"
                            }
                        }
                    } else {
                        echo "Environment config not set to deploy from branch, skipping"
                    }
                }
            }
        }
    }
}
