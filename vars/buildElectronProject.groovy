#!/usr/bin/groovy

import com.coral.epos2.Utils

def call(body) {
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    def utils = new Utils()
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "buildElectronProject: ${config.toString()}"

    node('Epos2Electron') {
        try {
            defaultBuildProperties{}

            stage('Checkout') {
                echo "Checking out branch: ${env.BRANCH_NAME} for build ${env.BUILD_NUMBER}"
                checkout scm
            }

            stage('Install') {
                def nodeHome = tool name: 'Node-8.1.3', type: 'jenkins.plugins.nodejs.tools.NodeJSInstallation'
                env.PATH = "${nodeHome}/bin:${env.PATH}"
                sh 'npm install'
            }

            def folder = "/coral-epos2-electron/${env.BRANCH_NAME}/${env.BUILD_NUMBER}"
            def releaseServerBaseUri = "http://electron-releases.coral-epos2.co.uk"

            buildElectronArtifactAndDeployToS3 {
                environment = "proxy"
                hostname = "localhost:5000"
                socketsProtocol = "ws"
                apiProtocol = "http"
                releaseFolder = "${folder}"
            }

            buildElectronArtifactAndDeployToS3 {
                environment = "dev0"
                hostname = "api.dev0.retail.dev.cloud.ladbrokescoral.com"
                socketsProtocol = "wss"
                apiProtocol = "https"
                releaseFolder = "${folder}"
            }

            buildElectronArtifactAndDeployToS3 {
                environment = "tst2"
                hostname = "api.tst2.retail.dev.cloud.ladbrokescoral.com"
                socketsProtocol = "wss"
                apiProtocol = "https"
                releaseFolder = "${folder}"
            }

            buildElectronArtifactAndDeployToS3 {
                environment = "test2"
                hostname = "mvp-test2-api.coral-epos2.co.uk"
                socketsProtocol = "wss"
                apiProtocol = "https"
                releaseFolder = "${folder}"
            }

            // only build staging if its the master branch
            if ('master' == env.BRANCH_NAME) {
                buildElectronArtifactAndDeployToS3 {
                    environment = "staging1"
                    hostname = "mvp-staging1-api.coral-epos2.co.uk"
                    socketsProtocol = "wss"
                    apiProtocol = "https"
                    releaseFolder = "${folder}"
                }
            }

            slackSend(color: '#FFFF00', message: "New electron release :parrotcop: - ${releaseServerBaseUri}${folder}")

            echo "Clean up workspace"
            deleteDir()
        } catch (error) {
            // send default error to main ci channel
            utils.notifySlackBuildFailed(utils.getDefaultCIChannel(), "${env.JOB_NAME}", "${env.BUILD_NUMBER}", "${env.BUILD_URL}")

            // try and send error message to a channel matching the branch name
            utils.notifySlackBuildFailed("${env.BRANCH_NAME}".toLowerCase(), "${env.JOB_NAME}", "${env.BUILD_NUMBER}", "${env.BUILD_URL}")

            throw error
        }
    }
}