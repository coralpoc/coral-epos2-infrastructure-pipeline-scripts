#!/usr/bin/groovy

def call(body) {
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "ecsDeploy: ${config.toString()}"

    def awsProfile = config.awsProfile // aws profile to use
    def clusterName = config.clusterName // ecs cluster deploying to
    def clusterService = config.clusterService // ecs cluster service being deployed to
    def image = config.image // image to deploy
    def maxDefinitions = 5 // number of ecs task definitions to keep (old ones will be archived)
    def timeout = 300 // amount of time we give ecs to respond to with result of deployment

    def command = "~/build-tools/bin/ecs-deploy.sh"

    if (awsProfile) {
        command += " -p ${awsProfile} "
    }

    if (timeout) {
        command += " --timeout ${timeout} "
    }

    if (maxDefinitions) {
        command += " --max-definitions ${maxDefinitions} "
    }

    sh "${command} -c ${clusterName} -n ${clusterService} -i ${image}"
}