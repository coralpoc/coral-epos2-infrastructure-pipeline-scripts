#!/usr/bin/groovy

import com.coral.epos2.Utils

def call(body) {
  // evaluate the body block, and collect configuration into the object
  def config = [:]
  def utils = new Utils()
  def buildCancelled = false;
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  echo "buildLocalProxycoreProject: ${config.toString()}"

  node {
    try {
      defaultBuildProperties{}

        stage('Checkout') {
          echo "Checking out branch: ${env.BRANCH_NAME} for build ${env.BUILD_NUMBER}"
          deleteDir()
          checkout scm
        }

        def folder = "/coral-epos2-local-proxy-core/${env.BRANCH_NAME}/${env.BUILD_NUMBER}"

        buildLocalProxyCoreMsiAndDeployToS3 {
          releaseFolder = "${folder}"
        }

        if (env.BRANCH_NAME == "master") {
          stage('Signing Required') {
            def userInput = true
            def didTimeout = false
            try {
                //Notify signing is required
                utils.notifySigningRequired(utils.getSigningChannel(), "${env.JOB_NAME}", "${env.BUILD_NUMBER}", "${env.BUILD_URL}")

                timeout(time: 1000, unit: 'DAYS') { // change to a convenient timeout for you
                    userInput = input(
                    id: 'Proceed1', message: 'Have you signed and uploaded the signed master build?', parameters: [
                    [$class: 'BooleanParameterDefinition', defaultValue: true, description: '', name: 'Please confirm you have signed the MSI']
                    ])
                }
            } catch(err) { // timeout reached or input false
                def user = err.getCauses()[0].getUser()
                if('SYSTEM' == user.toString()) { // SYSTEM means timeout.
                    didTimeout = true
                    buildCancelled = true
                } else {
                    userInput = false
                    buildCancelled = true
                    echo "Aborted by: [${user}]"
                }
            }
          }
        }
    
        stage('Notify'){
          if (!buildCancelled) {
            def releaseServerBaseUri = "http://electron-releases.coral-epos2.co.uk"
            slackSend (color: '#FF69B4 ', message: "New local proxy release :parrotdad: - ${releaseServerBaseUri}${folder}")
          }
        }
        
    } catch (error) {
      // send default error to main ci channel
      utils.notifySlackBuildFailed(utils.getDefaultCIChannel(), "${env.JOB_NAME}", "${env.BUILD_NUMBER}", "${env.BUILD_URL}")

      // try and send error message to a channel matching the branch name
      utils.notifySlackBuildFailed("${env.BRANCH_NAME}".toLowerCase(), "${env.JOB_NAME}", "${env.BUILD_NUMBER}", "${env.BUILD_URL}")

      throw error
    }
  }
}
