#!/usr/bin/groovy

def call(body) {
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "defaultBuildProperties: ${config.toString()}"

    properties([
//            // build every 5 minutes
//            pipelineTriggers([[$class: 'BitBucketTrigger'], pollSCM('H/5 * * * *')]),

            // discard old builds up to the latest 5
            buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '5')),

            // stop builds running concurrently
            disableConcurrentBuilds()
    ])
}