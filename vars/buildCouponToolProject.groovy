#!/usr/bin/groovy

import com.coral.epos2.Utils

def call(body) {
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    def utils = new Utils()
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "Build Coupon Population tool project: ${config.toString()}"

    node {
        try {
            // Ideally needs to be moved under Jankins settings to avoid changing workspace for each project in the script
            def workspace = env.WORKSPACE.replace("C:\\Program Files (x86)\\Jenkins\\workspace\\","C:\\Jks\\")
            ws (workspace) {
                defaultBuildProperties{}

                stage('Checkout') {
                    echo "Checking out branch: ${env.BRANCH_NAME} for build ${env.BUILD_NUMBER}"
                    deleteDir()
                    checkout scm
                }

                def folder = "/coral-epos2-coupon-tool/${env.BRANCH_NAME}/${env.BUILD_NUMBER}"

                buildCouponToolBinariesAndDeployToS3 {
                    releaseFolder = "${folder}"
                }

                def releaseServerBaseUri = "http://electron-releases.coral-epos2.co.uk"
                slackSend (color: '#FF69B4 ', message: "New Coupon Population tool release :dealwithitparrot: - ${releaseServerBaseUri}${folder}")
            }
        } catch (error) {
            // send default error to main ci channel
            utils.notifySlackBuildFailed(utils.getDefaultCIChannel(), "${env.JOB_NAME}", "${env.BUILD_NUMBER}", "${env.BUILD_URL}")

            // try and send error message to a channel matching the branch name
            utils.notifySlackBuildFailed("${env.BRANCH_NAME}".toLowerCase(), "${env.JOB_NAME}", "${env.BUILD_NUMBER}", "${env.BUILD_URL}")

            throw error
        }
    }
}