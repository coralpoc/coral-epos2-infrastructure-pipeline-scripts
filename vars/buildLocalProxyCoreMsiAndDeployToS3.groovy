#!/usr/bin/groovy

def call(body) {
  // evaluate the body block, and collect configuration into the object
  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  echo "buildLocalProxyCoreMsiAndDeployToS3: ${config.toString()}"

  def branchName = env.BRANCH_NAME.replaceAll('/','-')
  def bucketName = "coral-epos2-electron-releases"
  def path = "C:\\nuget\\;C:\\Program Files\\dotnet\\;${env.PATH}"
  def proxyHome = ".\\src\\Epos2LocalProxyCore"
  def releaseFolder = "${config.releaseFolder}"
  def wixCsproj = ".\\SetupWix\\SetupWix.csproj"
  def wixDirectory = "C:\\WixSharp.1.0.44.0"
  def wixBin = "C:\\WixSharp.1.0.44.0\\Wix_bin\\bin"

  echo "branchName: ${branchName}"

  stage("Build local proxy msi") {
    env.PROXY_BUILD_NUMBER  = "${env.BUILD_NUMBER}"
    env.PROXY_BRANCH = "${branchName}"
    if (branchName == "master" ){
        env.UNSIGNED = "-unsigned"
    }
    
    // ensure dotnet and nuget are in path
    env.PATH = "${path}"
    env.WIXSHARP_DIR = "${wixDirectory}"
    env.WIXSHARP_WIXDIR = "${wixBin}"

    bat 'nuget restore'
    bat "dotnet restore ${proxyHome}"
    bat "dotnet publish -c release -r win7-x86 ${proxyHome}"
    bat "msbuild ${wixCsproj}"
  }

  stage("Deploy msi artifact to S3 bucket ${bucketName}") {
      step([
              $class                              : 'S3BucketPublisher',
              dontWaitForConcurrentBuildCompletion: false,
              entries                             : [
                      [
                              bucket                 : "${bucketName}${releaseFolder}",
                              excludedFile           : '',
                              flatten                : false,
                              gzipFiles              : false,
                              keepForever            : false,
                              managedArtifacts       : false,
                              noUploadOnFailure      : true,
                              selectedRegion         : 'eu-west-1',
                              showDirectlyInBrowser  : false,
                              sourceFile             : "SetupWix/*.msi",
                              storageClass           : 'STANDARD',
                              uploadFromSlave        : false,
                              useServerSideEncryption: false
                      ]
              ],
              profileName                         : 'Jenkins S3 Profile',
              userMetadata                        : []
      ])
  }
}
