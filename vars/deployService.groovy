#!/usr/bin/groovy

def call(body) {
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "deployService: ${config.toString()}"

    def awsProfileToUse = config.awsProfile // aws profile to deploy with
    def clusterNameToDeploy = config.clusterName // ecs cluster that hosts the services
    def serviceNameToDeploy = config.serviceName // ecs cluster service we are updating
    def imageToDeploy = config.imageName // ecr image to deploy

    ecsDeploy {
        awsProfile = awsProfileToUse
        clusterName = clusterNameToDeploy
        clusterService = serviceNameToDeploy
        image = imageToDeploy
    }
}