#!/usr/bin/groovy

import com.coral.epos2.Utils

/**
 * Groovy script to build a spring boot project and deploy a service if necessary
 *
 * Requires a service deployment config in the format:
 *     serviceConfig = [
 *          name        : 'my-service',
 *          repoName    : 'my-service-repo',
 *          environments: [
 *              [
 *                  environment: 'dev',
 *                  autoDeploy       : true,
 *                  clusterName      : 'my-dev-services',
 *                  serviceName      : 'my-service',
 *                  dockerVersionName: 'develop'
 *              ],
 *              [
 *                  environment      : 'test1',
 *                  autoDeploy       : true,
 *                  clusterName      : 'my-test1-services',
 *                  serviceName      : 'my-service',
 *                  dockerVersionName: 'develop'
 *              ],
 *              [
 *                  environment      : 'staging',
 *                  autoDeploy       : false,
 *                  clusterName      : 'my-staging-services',
 *                  serviceName      : 'my-service',
 *                  dockerVersionName: 'master'
 *              ]
 *          ]
 *      ]
 *
 */
def call(body) {
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    def utils = new Utils()
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "buildSpringBootProject: ${config.serviceConfig.toMapString()}"

    echo "jobName: " + env.JOB_NAME
    echo "buildNumber: " + env.BUILD_NUMBER
    echo "buildUrl: " + env.BUILD_URL

    node('Epos2') {
        try {
            defaultBuildProperties {}

            env.JAVA_HOME = "${tool name: 'Java 8', type: 'jdk'}"
            env.PATH = "${env.JAVA_HOME}/bin:${env.PATH}"

            def MVN_HOME = tool name: 'Maven-3.3.9', type: 'hudson.tasks.Maven$MavenInstallation'

            stage('Checkout') {
                checkout scm
            }

            stage('Build') {
                sh "${MVN_HOME}/bin/mvn clean install -Dgit.branch.name=${env.BRANCH_NAME}"
            }

            if (env.BRANCH_NAME == 'develop') {
                stage('SonarQube analysis') {
                    withSonarQubeEnv('Sonarqube') {
                        // requires SonarQube Scanner for Maven 3.2+
                        sh "${MVN_HOME}/bin/mvn clean org.jacoco:jacoco-maven-plugin:prepare-agent install -Dgit.branch.name=${env.BRANCH_NAME}"
                        sh "${MVN_HOME}/bin/mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.2:sonar"
                    }
                }
            }

            deployToEnvironment {
                dockerRepo = config.serviceConfig.dockerRepo
                environments = config.serviceConfig.environments
            }

            echo "Clean up workspace"
            deleteDir()
        } catch (error) {
            // send default error to main ci channel
            utils.notifySlackBuildFailed(utils.getDefaultCIChannel(), "${env.JOB_NAME}", "${env.BUILD_NUMBER}", "${env.BUILD_URL}")

            // try and send error message to a channel matching the branch name
            utils.notifySlackBuildFailed("${env.BRANCH_NAME}".toLowerCase(), "${env.JOB_NAME}", "${env.BUILD_NUMBER}", "${env.BUILD_URL}")

            throw error
        }
    }
}