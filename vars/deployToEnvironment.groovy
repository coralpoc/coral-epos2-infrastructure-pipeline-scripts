#!/usr/bin/groovy

def call(body) {
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "deployToEnvironment: ${config.toString()}"

    def projectType = config.projectType // type of project being deploy java or node
    def appDir = config.appDir // folder with the deployable app
    def dockerRepo = config.dockerRepo // docker repo for the project
    def environments = config.environments // environment configs for the service

    def environmentsToBuild = []

    // go through environment configs and check what needs to be deployed
    for (environment in environments) {
        echo "Found environment config: ${environment.toString()}"

        if (environment.branch == env.BRANCH_NAME) {
            echo "Environment config set to build from branch: ${environment.toString()}"
            environmentsToBuild.push(environment)
        }
    }

    // if we have environments to build to then create image
    if (!environmentsToBuild.empty) {
        def repoBaseToDeploy = "550357753517.dkr.ecr.eu-west-1.amazonaws.com"
        def repoNameToDeploy = dockerRepo
        def versionNameToDeploy = "${env.BRANCH_NAME}"
        versionNameToDeploy = versionNameToDeploy.replaceAll('/','-')

        def buildDirectory = "./"

        // most of the projects are java so we control all the docker images from here
        // we have some custom node apps that just have their own docker files
        if (projectType == 'java') {
            echo "Creating build dir"
            sh 'mkdir -p build'

            echo "Copying artifact from ${appDir}"
            sh "cp ${appDir}/target/${appDir}-develop-SNAPSHOT.jar build/build.jar"

            echo "Copying Coral RAS certificate"
            def sourceCert = libraryResource 'com/coral/epos2/CORALSUBCA01.crt'
            writeFile file: 'build/CORALSUBCA01.crt', text: sourceCert

            echo "Copy Symantec certificate for UCMS"
            def symantecCert = libraryResource 'com/coral/epos2/SYMANTECCLASS3SECURESERVERCA-G4.crt'
            writeFile file: 'build/SYMANTECCLASS3SECURESERVERCA-G4.crt', text: symantecCert

            echo "Copying docker template"
            def dockerTemplate = libraryResource 'com/coral/epos2/Dockerfile-java-newrelic'
            writeFile file: 'build/Dockerfile', text: dockerTemplate

            buildDirectory = "build"
        }

        dir(buildDirectory) {
            stage('Building docker image') {
                buildDockerImage {
                    repoBase = "${repoBaseToDeploy}"
                    repoName = "${repoNameToDeploy}"
                    dockerCredentialsId = "ecr:eu-west-1:aws-ops-credentials"
                    dockerVersionName = "${versionNameToDeploy}"
                }
            }
        }

        // builds to staging are always from master branch
        // dev1 and test2 are from develop branches or EPOS- prefixed branches
        // test1 is build from release- prefix branches
        for (environment in environmentsToBuild) {
            if ((environment.name == 'staging1' && env.BRANCH_NAME.toLowerCase() == 'master')
                    || ((environment.name == 'dev1' || environment.name == 'test2') && (env.BRANCH_NAME.toLowerCase() == 'develop' || env.BRANCH_NAME.toLowerCase().startsWith("epos-")))
                    || (environment.name == 'test1' && env.BRANCH_NAME.toLowerCase().startsWith("release-"))) {
                // if this branch is configured to deploy to the environment then deploy
                if (environment.autoDeploy) {
                    echo "Environment config set to deploy from branch..."

                    def awsProfileToUse = environment.awsProfile
                    def serviceNameToDeploy = environment.serviceName
                    def clusterNameToDeploy = environment.clusterName
                    def customdeploy = environment.customdeploy

                    stage('Deploy ' + environment.name) {
                        // if there is a custom build script then just call it, this is to handle old legacy ec2 builds
                        if (customdeploy) {
                            sh "${customdeploy}"
                        } else {
                            deployService {
                                awsProfile = awsProfileToUse
                                serviceName = serviceNameToDeploy
                                clusterName = clusterNameToDeploy
                                imageName = "${repoBaseToDeploy}/${repoNameToDeploy}:${versionNameToDeploy}"
                            }
                        }
                    }
                } else {
                    echo "Environment config not set to deploy from branch, skipping"
                }
            }
        }

        echo "Removing docker images"
        sh "docker rmi ${repoNameToDeploy}:${versionNameToDeploy}"
        sh "docker rmi ${repoBaseToDeploy}/${repoNameToDeploy}:${versionNameToDeploy}"
    }
}
