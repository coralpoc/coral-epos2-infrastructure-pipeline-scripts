#!/usr/bin/groovy

def call(body) {
    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "Build Coupon Population tool project and deploy output to S3: ${config.toString()}"

    def branchName = env.BRANCH_NAME.replaceAll('/','-')
    def bucketName = "coral-epos2-electron-releases"
    def path = "C:\\nuget\\;C:\\Program Files\\dotnet\\;${env.PATH}"
    def releaseFolder = "${config.releaseFolder}"
    def cptSln = ".\\CPTv2.sln"
    def versionNumber = "10.0.0.${env.BUILD_NUMBER}"

    echo "branchName: ${branchName}"

    stage("Build Coupon Population tool") {
        env.PROXY_BUILD_NUMBER  = "${env.BUILD_NUMBER}"
        env.PROXY_BRANCH = "${branchName}"
        // ensure dotnet and nuget are in path
        env.PATH = "${path}"

        bat 'nuget restore CPTv2.sln'
		
        def versionFile = new File("${pwd()}\\GlobalAssemblyInfo.cs")
        versionFile.write(versionFile.text.replace("7.0.0.1", "${versionNumber}"))

        bat "msbuild ${cptSln} /t:Build /p:Configuration=Release"
    }

    stage("Zip build output") {
        bat '"C:\\Program Files\\7-Zip\\7z.exe" a -r Coupon_Tool.zip ".\\Release\\*.*" -mem=AES256'
    }

    stage("Deploy build artefacts to S3 bucket ${bucketName}") {
        step([
            $class                              : 'S3BucketPublisher',
            dontWaitForConcurrentBuildCompletion: false,
            entries                             : [
                [
                    bucket                 : "${bucketName}${releaseFolder}",
                    excludedFile           : '',
                    flatten                : false,
                    gzipFiles              : false,
                    keepForever            : false,
                    managedArtifacts       : false,
                    noUploadOnFailure      : true,
                    selectedRegion         : 'eu-west-1',
                    showDirectlyInBrowser  : false,
                    sourceFile             : "*.zip",
                    storageClass           : 'STANDARD',
                    uploadFromSlave        : false,
                    useServerSideEncryption: false
                ]
            ],
            profileName                         : 'Jenkins S3 Profile',
            userMetadata                        : []
        ])
    }
}
